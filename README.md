# MIT NEDBDay 2019 Poster

The up-to-date version of the poster is built in CI and resides as artifact.

- [Download the file](https://git.dbogatov.org/bu/ore-benchmark/mit-poster/-/jobs/artifacts/master/raw/poster.pdf?job=artifacts)
- [View the file](https://git.dbogatov.org/bu/ore-benchmark/mit-poster/-/jobs/artifacts/master/file/poster.pdf?job=artifacts)

## How to compile

```bash
./document/build.sh # to compile "for release"
open ./document/dist/*.pdf # to open
```
