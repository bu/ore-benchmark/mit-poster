% cSpell:ignore totalwidth twocolwid onecolwid sepwid ngreen nblue setbeamercolor leqnomode OCPA PRFs POPF ESSA Bogatova

\begin{column}{\onecolwid}

	\setbeamercolor{block title}{fg=ngreen,bg=white}
	\setbeamercolor{item}{fg=ngreen}

	\begin{block}{Abstract}
	
		Database query evaluation over encrypted data has received a lot of attention recently.
		Order Preserving Encryption (OPE) and Order Revealing Encryption (ORE) are two important encryption schemes that have been proposed in this area.
		These schemes can provide very efficient query execution, but at the same time may leak some information to adversaries. 
		More protocols have been introduced that are based on Searchable Symmetric Encryption (SSE), Oblivious RAM (ORAM) or custom encrypted data structures.
		We present the first comprehensive comparison among a number of important secure range query protocols using a framework that we developed.
		We evaluate five ORE-based and five generic range query protocols.
		We analyze and compare them both theoretically and experimentally and measure their performance over database indexing and query evaluation.
		We report not only execution time but also {\IO} performance, communication amount, and usage of cryptographic primitive operations.
		Our comparison reveals some interesting insights concerning the relative security and performance of these approaches in database settings.

	\end{block}

	\setbeamercolor{block title}{fg=nblue,bg=white}
	\setbeamercolor{item}{fg=nblue}
	
	\begin{block}{Order-Preserving Encryption}

		An Order-\emph{Preserving} Encryption (OPE) scheme is a tuple of polynomial-time algorithms $\setup$, $\encrypt$, $\decrypt$ defined over a well-ordered domain $\domain$ with the following properties:
		
		\begin{itemize}
			\item 
				$\setup(1^\lambda) \to \sk$.
				On input a security parameter $\lambda$ (formally, in unary, to ensure polynomial running time), the randomized setup algorithm $\setup$ outputs a secret key $\sk$.
			\item 
				$\encrypt(\sk, m) \to \ct$.
				On input the secret key $\sk$ and a message $m \in \mathcal{D}$, the possibly randomized encryption algorithm $\encrypt$ outputs a \emph{numerical} ciphertext $\ct$.
			\item
				$\decrypt(\sk, \ct) \to m$.
				On input the secret key $\sk$ and a numerical ciphertext \ct, the deterministic decryption algorithm $\decrypt$ outputs the original message $m$.
		\end{itemize}
		
		OPE scheme is correct when $\decrypt$ is correct
		\[
			\Pr [ \decrypt( \sk, \encrypt( \sk, m) ) = m ] = 1 - \negl(\lambda)
		\]
		and when the order of ciphertexts is preserved
		\[
			\Pr [ \bm{P}(m_1, m_2) = \bm{P}(\ct_1, \ct_2) ] = 1 - \negl(\lambda)
		\]
		for $\bm{P}$ being a comparison operator ($<$, $\le$, $=$, $\ge$, $>$).
		
	\end{block}
	
	\begin{block}{Order-Revealing Encryption}
		
		An Order-\emph{Revealing} Encryption (ORE) scheme is different from OPE in the format of ciphertexts and public keyless $\compare$ routine
		
		\begin{itemize}
			\item 
				$\encrypt(\sk, m) \to \ct$.
				On input the secret key $\sk$ and a message $m \in \mathcal{D}$, the possibly randomized encryption algorithm $\encrypt$ outputs a \emph{non-numeric} ciphertext $\ct$.
			\item
				$\compare(\ct_1, \ct_2) \to b$.
				On input two ciphertexts $\ct_1$, $\ct_2$, the comparison algorithm $\compare$ outputs a bit $b \in \{ 0, 1 \}$.
		\end{itemize}
		
		ORE is correct when $\compare$ is correct
		\[
			\Pr [ \compare(\ct_1, \ct_2) = \bm{1}(m_1 < m_2) ] = 1 - \negl(\lambda)
		\]
		for $\bm{1}(\cdot)$ being $1$ when the condition is true.
		
		To decrypt, having a secret key one can either do a binary search over domain, or attach a symmetric encryption to the ciphertext and use it for decryption.
		
	\end{block}
	
	\begin{block}{ORE-based Secure Range Query protocol}
		
		For an ORE scheme $\ore = (\setup, \encrypt, \compare)$, symmetric encryption scheme $\symmetric = (\setup, \allowbreak \encrypt, \decrypt)$ and data structure $\ds = (\ins, \search)$, the protocol $\Pi$ is defined as follows.
		\begin{itemize}
			\item $\Pi.\handshake$:
				\begin{itemize}
					\item[--] Generate $K = \ore.\setup(\cdot)$ and $k = \symmetric.\setup(\cdot)$
					\item[--] Initialize $\ds$
				\end{itemize}
			\item $\Pi.\ins (i, v)$:
				\begin{itemize}
					\item[--] Client encrypts a data point index $\bm{i} = \ore.\encrypt(K, i)$
					\item[--] Client encrypts a data point value $\bm{v} = \symmetric.\encrypt(k, v)$
					\item[--] Client sends $\{ \bm{i}, \bm{v} \}$ to the server
					\item[--] Server stores the encrypted data point $\ds.\ins( \{ \bm{i}, \bm{v} \} )$
				\end{itemize}
			\item $\Pi.\search (l, r)$:
				\begin{itemize}
					\item[--] Client encrypts the endpoints $\{ \bm{l}, \bm{r} \} = \ore.\encrypt(K, \{ l, r \})$
					\item[--] Client sends $\{ \bm{l}, \bm{r} \}$ to the server
					\item[--] Server answers the query $\bm{r} = \ds.\search(\{ \bm{l}, \bm{r} \})$
					\item[--] Server sends the result $\bm{r}$ back to the client
					\item[--] Client decrypts all elements $r = \symmetric.\decrypt(k, \bm{r})$
				\end{itemize}
		\end{itemize}

	\end{block}
	
\end{column}
	
\begin{column}{\twocolwid}
	
	\setbeamercolor{block title}{fg=nblue,bg=white}
	\setbeamercolor{item}{fg=nblue}

	\begin{block}{ORE schemes primitive usage}

		\input{figures/tbl-primitives-theory}

	\end{block}

	\begin{block}{Protocols performance values}

		\input{figures/tbl-protocols-theory}

	\end{block}

	\begin{block}{Protocols descriptions}

		\setlength\columnsep{50pt}

		\begin{multicols}{2}
		
			\begin{description}
				\justifying%
				\item[ORE with {\BPlus} tree.]
					In essence, this construction is a regular {\BPlus} tree with ORE ciphertexts as indices and $\compare$ routine built in.
					This construction's strength is its {\IO} optimization --- $\BigO{\log_B \left( \nicefrac{N}{B} \right) + \nicefrac{r}{B}}$.
					
					\begin{figure}
						\includegraphics[width=\linewidth]{b-plus-tree.pdf}
					\end{figure}
					
				\item[Kerschbaum-Tueno~\cite{florian-protocol}.]
					This construction maintains an array of symmetrically encrypted indices on the server, in-order, but with applied modular rotation.
					When inserting or searching, server interactively traverses the structure like a binary tree asking client for a direction to go.
					Each time a new element is inserted, a structure is rotated incurring massive {\IO} overhead.
					
					\begin{figure}
						\includegraphics[width=\linewidth]{florian-protocol.pdf}
					\end{figure}
					
				\item[POPE~\cite{pope}.]
					This construction is based on buffer trees to support fast insertion and lazy sorting.
					All indices are symmetrically encrypted and server asks the client to sort a list of ciphertexts thus structuring the tree.
					New elements are always inserted in the root's buffer, and during queries are pushed down to the leaves.
					This construction incurs massive {\IO} overhead on first query (cold start) and its nodes are not optimized for {\IO} page size.
					
					\begin{figure}
						\includegraphics[width=\linewidth]{pope.pdf}
					\end{figure}

				\item[Logarithmic BRC usnig SSE~\cite{ioannis-protocol}.]
					This construction builds a virtual binary tree over the input domain and assigns each input element keywords from the path from this element to the root.
					This keyword-index mapping is encrypted with SSE\@.
					On query, a client finds the minimal number of nodes that cover the range and queries the SSE server for these keywords.
					This construction's {\IO} performance is that of SSE --- linear in result size.
					
					\vspace{0.5in}
					
					\begin{figure}
						\includegraphics[width=\linewidth]{sse.pdf}
					\end{figure}
					
					\vspace{0.5in}

				\item[{\BPlus} tree in ORAM.]
					A client operates on a regular {\BPlus} tree, but each time a node is accessed it is read or written to the ORAM server.
					This effectively squares the {\IO} usage since for each node in the logarithmic path, there is a logarithmic overhead in ORAM\@.
					This construction, however, is the most secure --- additionally to data it hides the access pattern.
					
					\vspace{0.5in}
					
					\begin{figure}
						\includegraphics[width=\linewidth]{oram.pdf}
					\end{figure}

			\end{description}

		\end{multicols}
		
	\end{block}

\end{column}
	
\begin{column}{\twocolwid}
	
	\setbeamercolor{block title}{fg=nred,bg=white}
	\setbeamercolor{item}{fg=nred}
	
	\begin{block}{Our results}
	
		\begin{alertblock}{Performance values for different data distributions}
	
			\input{figures/fig-protocols}

		\end{alertblock}

	\end{block}
	
	\vspace{-0.5in}
	
	\begin{columns}[t,totalwidth=\twocolwid]
	
		\begin{column}{\oneintwocolwid}
			
			\begin{block}{Schemes and primitives}

				\begin{alertblock}{Schemes and primitives benchmarks}

					\input{figures/fig-time-benchmark}

				\end{alertblock}

			\end{block}
			
			\begin{block}{Benchmark methodology}
			
				\begin{itemize}
					\item
						We have implemented almost all primitives, schemes, data structures and protocols ourselves.
						The code is written in {\Csharp} and runs on {.NET Core 2.2}.
						The code is tested with over a thousand unit tests and the coverage is above 97\%.
					\item
						Almost all primitives are based on AES\@.
					\item
						Faithful modeling of {\IO} using different caching policies --- LRU, LFU and FIFO\@.
					\item
						Time for primitive and schemes benchmark is measured with Benchmark.NET\@.
						{\IO} requests, primitive usage and communication are measured by firing events from within execution and carefully catching them.
					\item
						Synthetic data sets are generated pseudo-randomly and real one is California public employees salaries.
					\item
						All computations except rare symmetric encryptions are deterministic given global seed.
						All experiments can be reproduced exactly.
					\item
						Our tool is capable of generating massive detailed fine-grained reports for protocol executions.
						We present only the most interesting tiny fraction of the experimental results.
				\end{itemize}
			
			\end{block}
			
		\end{column}

		\begin{column}{\oneintwocolwid}
			
			\begin{block}{Conclusions}
			
				We have found that primitive usage is a much better performance measure than the plain time measurements.
				We have also found that {\IO} optimizations is a vital characteristic of a protocol and cannot be neglected.
				
				\begin{description}
					\justifying%
					\item[ORE with {\BPlus} tree]
						is tuneable in security / performance tradeoff.
						Index data structure and underlying ORE scheme can be replaced independently.
					\item[Kerschbaum protocol~\cite{florian-protocol}] 
						offers semantically secure ciphertexts, hides the location of the smallest and largest of them, has a simple implementation, but requires batch insertions.
					\item[POPE~\cite{pope}] 
						offers a ``deferred'' {\BPlus} tree implementation and remains more secure for the small number of queries.
						Incurs massive {\IO} hit on first queries and is not optimized for {\IO} like {\BPlus} tree.
					\item[Logarithmic BRC usnig SSE~\cite{ioannis-protocol}]
						relies on underlying SSE scheme's security and offers a different tradeoff --- performance as a function of the result size. 
						It is also not optimized for non-batch insertions.
					\item[ORAM]
						offers the strongest security.
						Performance hit, although heavy, is comparable with other protocols.
						ORAM server acts as a generic secure key-value store.
				\end{description}

			\end{block}
			
			\setbeamercolor{block title}{fg=ngreen,bg=white}
			\setbeamercolor{item}{fg=ngreen}
			
			\begin{block}{Acknowledgements}
			
				\small{
					\rmfamily{
						\section{Acknowledgments}
							We would like to thank Adam O'Neill, George Kellaris and Ioannis Demertzis for helpful discussions and Daria Bogatova for help with protocol diagrams.
							We also thank ESSA2 workshop organizers and attendees for in-depth discussions of their constructions.
							George Kollios and Dmytro Bogatov were supported by an NSF SaTC Frontier Award CNS-1414119.
							Leonid Reyzin was supported in part by NSF grant 1422965.
					}
				} \\
			
			\end{block}

			\begin{block}{References}
				
				\begin{multicols}{2}
			
					\tiny{
						\bibliographystyle{unsrt}
						\bibliography{bibliography}
					}
					
				\end{multicols}
			
			\end{block}
			
		\end{column}
		
	\end{columns}
	
\end{column}
